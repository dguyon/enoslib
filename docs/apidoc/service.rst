************
Service APIs
************

Base Service Class
==================

.. automodule:: enoslib.service.service
    :members:
    :undoc-members:
    :show-inheritance:

Conda
======

Conda Service Class
--------------------


.. automodule:: enoslib.service.conda.conda
    :members:
    :undoc-members:
    :show-inheritance:

Dask
====

Dask Service Class
--------------------


.. automodule:: enoslib.service.dask.dask
    :members:
    :undoc-members:
    :show-inheritance:

Docker
======

Docker Service Class
--------------------


.. automodule:: enoslib.service.docker.docker
    :members:
    :undoc-members:
    :show-inheritance:

Dstat (monitoring)
==================

Dstat Service Class
--------------------


.. automodule:: enoslib.service.dstat.dstat
    :members:
    :undoc-members:
    :show-inheritance:

Locust (Load generation)
========================

Locust Service Class
------------------------

.. automodule:: enoslib.service.locust.locust
    :members:
    :undoc-members:
    :show-inheritance:

Monitoring
==========

Monitoring Service Class
------------------------

.. _monitoring:


.. automodule:: enoslib.service.monitoring.monitoring
    :members:
    :undoc-members:
    :show-inheritance:


Network Emulation (Netem)
=========================

.. _netem:

Netem Service Class
-------------------


.. automodule:: enoslib.service.netem.netem
    :members:
    :undoc-members:
    :show-inheritance:


Skydive
=======

.. _skydive:

Skydive Service Class
---------------------


.. automodule:: enoslib.service.skydive.skydive
    :members:
    :undoc-members:
    :show-inheritance: